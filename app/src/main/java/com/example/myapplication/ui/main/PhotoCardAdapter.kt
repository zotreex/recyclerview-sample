package com.example.myapplication.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class PhotoCardAdapter :
    ListAdapter<PhotoCard, PhotoCardAdapter.ViewHolder>(ItemDiffCallback()) {

    var listener: CardClickListener? = null

    class ViewHolder(view: View, listener: CardClickListener?) : RecyclerView.ViewHolder(view) {

        val cardTitle = view.findViewById<TextView>(R.id.card_title)
        val cardDescription = view.findViewById<TextView>(R.id.card_description)
        val cardIcon = view.findViewById<ImageView>(R.id.card_exit)

        fun bind(photoCard: PhotoCard, listener: CardClickListener?) {
            cardTitle.text = photoCard.title
            cardDescription.text = photoCard.description
            cardIcon.setOnClickListener {
                listener?.onClick(photoCard)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item, parent, false)

        return ViewHolder(view, listener)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position], listener)
    }
}

class ItemDiffCallback : DiffUtil.ItemCallback<PhotoCard>() {
    override fun areItemsTheSame(oldItem: PhotoCard, newItem: PhotoCard): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PhotoCard, newItem: PhotoCard): Boolean = oldItem == newItem

}

interface CardClickListener {
    fun onClick(card: PhotoCard)
}