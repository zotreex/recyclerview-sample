package com.example.myapplication.ui.main

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class MainFragment : Fragment(), CardClickListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val layout = R.layout.main_fragment
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onClick(card: PhotoCard) {
        Log.d("tag", card.toString())
        findNavController().navigate(
            R.id.toDetails, bundleOf(
                "cardID" to card.id
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.fastOptions)
        val adapter = FastOptionsAdapter()
        adapter.submitList(mutableListOf(41, 2, 4, 5, 9))
        recyclerView.adapter = adapter

        val cardView = view.findViewById<RecyclerView>(R.id.photoCards)
        val cardAdapter = PhotoCardAdapter()
        cardAdapter.listener = this
        cardAdapter.submitList(
            mutableListOf(
                PhotoCard(
                    id = 1,
                    title = "Test1",
                    description = "test2"
                ),
                PhotoCard(
                    id = 2,
                    title = "test3",
                    description = "test4"
                )
            )
        )
        cardView.adapter = cardAdapter
    }

}