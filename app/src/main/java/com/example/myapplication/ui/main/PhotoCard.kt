package com.example.myapplication.ui.main

data class PhotoCard(
    val id: Int,
    val title: String,
    val description: String,
    val url: String = "",
)
